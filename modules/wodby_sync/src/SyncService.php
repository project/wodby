<?php

namespace Drupal\wodby_sync;

use Drupal\Component\Serialization\Json;
use Drupal\wodby\WodbyClientServiceInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

/**
 * SyncService service.
 */
class SyncService {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Wodby API.
   *
   * @var \Drupal\wodby\WodbyClientServiceInterface
   */
  protected $wodbyApi;

  /**
   * Constructs a SyncService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\wodby\WodbyClientServiceInterface $wodby_api
   *   The Wodby API.
   */
  public function __construct(ClientInterface $http_client, WodbyClientServiceInterface $wodby_api) {
    $this->httpClient = $http_client;
    $this->wodbyApi = $wodby_api;
  }

  /**
   * Returns the default http headers for API calls.
   *
   * @return string[]
   *   The default http headers.
   */
  protected function getDefaultHeaders(): array {
    return [
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'Authorization' => 'token ' . getenv('WODBY_API_KEY'),
    ];
  }

  /**
   * Get the current cronjob configuration of the destination instance.
   *
   * @param string $destination_uuid
   *   The destination instance uuid.
   *
   * @return string
   *   The task UUID.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Errors in API requests.
   * @throws \RuntimeException
   *   Errors in login and api responses.
   */
  protected function getCronjobConfiguration(string $destination_uuid): string {
    // Retrieve the current configuration.
    $response = $this->httpClient->request(
      'GET',
      "https://api.wodby.com/api/v2/instances/{$destination_uuid}",
      [
        RequestOptions::HEADERS => $this->getDefaultHeaders(),
      ]
    );

    $data = Json::decode($response->getBody()->getContents());
    if (!empty($data['error'])) {
      throw new \RuntimeException('Disabling of cronjobs failed: ' . $data['error']);
    }
    if (!isset($data["properties"]['cron_tasks'])) {
      throw new \RuntimeException('Disabling of cronjobs failed: no task returned');
    }

    return $data["properties"]['cron_tasks'];
  }

  /**
   * Disable the destination instance.
   *
   * Disables the destination instance by enabling maintenance mode and
   * disabling the cronjobs.
   *
   * @param string $destination_uuid
   *   The destination instance uuid.
   *
   * @return string
   *   The task UUID.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Errors in API requests.
   * @throws \RuntimeException
   *   Errors in login and api responses.
   */
  public function disableInstance(string $destination_uuid): string {
    // Retrieve the current configuration.
    $cronjob_original_config = $this->getCronjobConfiguration($destination_uuid);

    // Set original cronjob configuration to be able to restore it afterwards
    // and on failure.
    \Drupal::state()->set('wodby_sync.cronjob_original_config', $cronjob_original_config);

    // Implode the cronjob configuration per line and prepend a string to
    // disable them.
    $cronjob_config = explode("\n", $cronjob_original_config);
    foreach ($cronjob_config as $lineNr => $line) {
      $cronjob_config[$lineNr] = '###TEMP DISABLE CRON###' . $line;
    }
    $disabled_cronjob_config = implode("\n", $cronjob_config);

    // Save storeds cronjob configuration.
    $response = $this->httpClient->request(
      'POST',
      "https://api.wodby.com/api/v2/instances/{$destination_uuid}/stack/config",
      [
        RequestOptions::HEADERS => $this->getDefaultHeaders(),
        RequestOptions::JSON => ['maintenance_mode' => TRUE, 'cron_tasks' => $disabled_cronjob_config],
      ]
    );

    $data = Json::decode($response->getBody()->getContents());
    if (!empty($data['error'])) {
      throw new \RuntimeException('Disabling of cronjobs failed: ' . $data['error']);
    }
    if (empty($data['task']['id'])) {
      throw new \RuntimeException('Disabling of cronjobs failed: no task returned');
    }

    return $data['task']['id'];
  }

  /**
   * Enable the destination instance.
   *
   * Enables the destination instance by disabling maintenance mode and
   * enabling the cronjobs.
   *
   * @param string $destination_uuid
   *   The destination instance uuid.
   *
   * @return string
   *   The task UUID.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Errors in API requests.
   * @throws \RuntimeException
   *   Errors in login and api responses.
   */
  public function enableInstance(string $destination_uuid): string {
    // Get original cronjob configuration that has been stored.
    $cronjob_original_config = \Drupal::state()->get('wodby_sync.cronjob_original_config');

    // Save stored cronjob configuration.
    $response = $this->httpClient->request(
      'POST',
      "https://api.wodby.com/api/v2/instances/{$destination_uuid}/stack/config",
      [
        RequestOptions::HEADERS => $this->getDefaultHeaders(),
        RequestOptions::JSON => ['maintenance_mode' => FALSE, 'cron_tasks' => $cronjob_original_config],
      ]
    );

    $data = Json::decode($response->getBody()->getContents());
    if (!empty($data['error'])) {
      throw new \RuntimeException('Enabling of cronjobs failed: ' . $data['error']);
    }
    if (empty($data['task']['id'])) {
      throw new \RuntimeException('Enabling of cronjobs failed: no task returned');
    }

    return $data['task']['id'];
  }

  /**
   * Starts the syncing process.
   *
   * @param string $source_uuid
   *   The source instance uuid
   * @param string $destination_uuid
   *   The destination instance uuid.
   *
   * @return string
   *   The task UUID.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Errors in API requests.
   * @throws \RuntimeException
   *   Errors in login and api responses.
   */
  public function startSync(string $source_uuid, string $destination_uuid): string {
    /*
     * So, there's that. :) The current api version 3 does not support the "sync
     *  between instances" action; thus we need to use the older API version 2.
     *
     * When the API v3 will support the "sync between instances" action, the code
     * below will look something like:
     *
     * $task = \Drupal::service('wodby.client')
     *   ->getInstanceApi()
     *   ->syncInstances($source_uuid, $destination_uuid);
     */
    $sync_data = [
      "database" => $source_uuid,
    ];
    $response = $this->httpClient->request(
      'POST',
      "https://api.wodby.com/api/v2/instances/{$destination_uuid}/import/instances",
      [
        RequestOptions::HEADERS => $this->getDefaultHeaders(),
        RequestOptions::JSON    => $sync_data,
      ]
    );

    $data = Json::decode($response->getBody()->getContents());
    if (!empty($data['error'])) {
      throw new \RuntimeException('Sync failed: ' . $data['error']);
    }
    if (empty($data['task']['id'])) {
      throw new \RuntimeException('Sync failed: no task returned');
    }

    return $data['task']['id'];
  }

  /**
   * Deploy the destination instance.
   *
   * @param string $destination_uuid
   *   The destination instance uuid.
   *
   * @return string
   *   The task UUID.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *   Errors in API requests.
   */
  public function deployInstance(string $destination_uuid): string {
    $task = $this->wodbyApi->deployInstance($destination_uuid, ['post_deployment' => TRUE]);
    return $task['task']['id'];
  }

  /**
   * Get the wodby task.
   *
   * @param string $task_uuid
   *   The task uuid.
   *
   * @return array
   *   The task information.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTask(string $task_uuid): array {
    $task = $this->wodbyApi->getTasks($task_uuid);

    return $task;
  }

}
