<?php

namespace Drupal\wodby_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Wodby Sync form.
 */
class InstanceSync extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sync_instances';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $step = $form_state->get('form_step');
    if (empty($step)) {
      $step = 1;
      $form_state->set('form_step', $step);
    }

    // There are several API calls, so catch any exception and display it.
    try {
      // This is a multistep form, select the correct current step.
      switch ($step) {
        case 2:
          $form = $this->buildStep2($form, $form_state);
          break;

        case 1:
        default:
          $form = $this->buildStep1($form, $form_state);
      }
    }
    catch (\RuntimeException $e) {
      $form['error'] = [
        '#type'  => 'fieldset',
        '#title' => $this->t('Error occurred'),
      ];
      $form['error']['info'] = [
        '#type'   => 'markup',
        '#markup' => $e->getMessage(),
      ];
    }

    return $form;
  }

  /**
   * Step 1: Select the source and destination instances.
   */
  public function buildStep1(array $form, FormStateInterface $form_state) {
    $form['sync_instances'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select instances to sync'),
      '#attributes' => [
        'id' => 'wodby-sync-wrapper',
      ],
    ];

    $form['sync_instances']['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source instance'),
      // Allow the user to select the production instances as source.
      '#options' => $this->getInstanceOptions(NULL, FALSE),
      '#default_value' => $form_state->get('source'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxStep1UpdateSelects',
        'wrapper' => 'wodby-sync-wrapper',
      ],
    ];

    // The destination instance depends on the source instance, thus will be loaded via ajax.
    $form['sync_instances']['destination'] = [
      '#type'     => 'select',
      '#title'    => $this->t('Destination instance'),
      '#description' => $this->t('As safety measure, the current instance AND production instances are not allowed as destination.'),
      '#options'  => [],
      '#disabled' => TRUE,
    ];
    $selected_uuid = $form_state->getValue('source');
    if (!empty($selected_uuid)) {
      $options = ['' => $this->t('- Select -')] + $this->getInstanceOptions($selected_uuid);
      $form['sync_instances']['destination'] = [
          '#options'       => $options,
          '#default_value' => $form_state->get('destination'),
          '#required'      => TRUE,
          '#disabled'      => FALSE,
        ] + $form['sync_instances']['destination'];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#button_type' => 'primary',
      '#submit' => ['::submitStep1'],
      '#validate' => ['::validateStep1'],
    ];

    return $form;
  }

  public function ajaxStep1UpdateSelects(&$form, FormStateInterface $form_state) {
    return $form['sync_instances'];
  }

  /**
   * Basic validation for step 1.
   */
  public function validateStep1(array &$form, FormStateInterface $form_state) {
    $source = $form_state->getValue('source');
    $destination = $form_state->getValue('destination');
    // Simply check if the source and destination are both set and different.
    if (empty($source)) {
      $form_state->setErrorByName('source', $this->t('Please select a source instance.'));
    }
    if (empty($destination)) {
      $form_state->setErrorByName('destination', $this->t('Please select a destination instance.'));
    }
    if ($source && $destination && $source === $destination) {
      $form_state->setErrorByName('destination', $this->t('You cannot select the same instance as source and destination.'));
    }
  }

  /**
   * Submit handler for step 1.
   */
  public function submitStep1(array &$form, FormStateInterface $form_state) {
    // Store the selected instances in the form state for later use.
    $form_state->set('step_values', [
      'source'      => $form_state->getValue('source'),
      'destination' => $form_state->getValue('destination'),
    ])
      // Set the next step to 2.
      ->set('form_step', 2)
      ->setRebuild(TRUE);
  }

  /**
   * Step 2: Asks the user for confirmation.
   */
  public function buildStep2(array $form, FormStateInterface $form_state) {
    $form_state->set('form_step', 2);

    $form['warning'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Warning'),
    ];

    // TODO: Review the text for the warning and make it more informative.
    $form['warning']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('This will synchronize all content from the source instance to the destination instance. This can take a long time and is not reversible. Please make sure you have a backup of the destination instance before proceeding.<br>Please do not close your browser window and interrupt the process.'),
    ];

    $form['warning']['accept_risk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I understand the consequences of this action.'),
      '#required' => TRUE,
      '#description' => $this->t('Please check this box to confirm that you understand the consequences of this action.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start sync'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('accept_risk')) {
      // TODO: Better error message perhaps?
      $form_state->setErrorByName('accept_risk', $this->t('Please confirm that you understand the consequences of this action.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sync_info = $form_state->get('step_values');

    try {
      $operations = [
        [
          'wodby_sync_batch_disable_instance',
          [$sync_info['destination']],
        ],
        [
          'wodby_sync_batch_check_task',
          ['disable_instance'],
        ],
        [
          'wodby_sync_batch_start_sync',
          [$sync_info['source'], $sync_info['destination']],
        ],
        [
          'wodby_sync_batch_check_task',
          ['sync'],
        ],
        [
          'wodby_sync_batch_deploy_instance',
          [$sync_info['destination']],
        ],
        [
          'wodby_sync_batch_check_task',
          ['deploy'],
        ],
        [
          'wodby_sync_batch_enable_instance',
          [$sync_info['destination']],
        ],
        [
          'wodby_sync_batch_check_task',
          ['enable_instance'],
        ],
      ];
      $batch = [
        'operations' => $operations,
        'title' => t('Syncing instances'),
        'progress_message' => '',
        'error_message' => t('The synchronization between the instances failed.'),
        'finished' => 'wodby_sync_batch_finished',
        'file' => \Drupal::service('extension.list.module')->getPath('wodby_sync') . '/wodby_sync.batch.inc',
      ];
      batch_set($batch);
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

  }

  /**
   * Load the instances from the API and return them as select options.
   *
   * @param string|null $except_uuid
   *   Exclude the specified instance from the list.
   *
   * @param bool $exclude_prod
   *   Exclude production instances, default TRUE.
   *
   * @return array
   *   An array of instances keyed by uuid.
   */
  protected function getInstanceOptions(string $except_uuid = NULL, bool $exclude_prod = TRUE): array {
    $options = [];

    /** @var \Drupal\wodby\WodbyClientServiceInterface $service */
    $service = \Drupal::service('wodby.client');
    $app_id = getenv('WODBY_APP_UUID');
    $instances = $service->getInstances($app_id);

    foreach ($instances as $instance) {
      // Skip explicitly excluded instance.
      if ($except_uuid && $instance['id'] === $except_uuid) {
        continue;
      }
      // Skip production instances unless explicitly allowed.
      if ($exclude_prod && $instance['type'] === 'prod') {
        continue;
      }
      $options[$instance['id']] = $this->t('@name (Type: @type)', [
        '@name' => $instance['title'],
        '@type' => $instance['type'],
      ]);
    }
    return $options;
  }

}
