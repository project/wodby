<?php

/**
 * @file
 * Batch process sync the database between two instances.
 */

/**
 * Implements callback_batch_operation().
 *
 * Disables the destination instance by enabling the maintenance mode and
 * disable the cronjobs.
 *
 * @param string $instance_uuid
 *   The uuid of the destination instance.
 * @param array $context
 *   The batch context.
 */
function wodby_sync_batch_disable_instance($instance_uuid, &$context) {
  try {
    /** @var \Drupal\wodby_sync\SyncService $syncService */
    $syncService = \Drupal::service('wodby_sync.sync_service');
    $task_uuid = $syncService->disableInstance($instance_uuid);
    \Drupal::state()->set('wodby_sync.current_task_id', $task_uuid);
    $context['message'] = t('Disabling destination instance for data sync.');
  }
  catch (Exception $e) {
    $context['results']['errors'][] = $e->getMessage();
  }
}

/**
 * Implements callback_batch_operation().
 *
 * Enables the destination instance by disabling the maintenance mode and
 * enabling the cronjobs.
 *
 * @param string $instance_uuid
 *   The uuid of the destination instance.
 * @param array $context
 *   The batch context.
 */
function wodby_sync_batch_enable_instance($instance_uuid, &$context) {
  try {
    /** @var \Drupal\wodby_sync\SyncService $syncService */
    $syncService = \Drupal::service('wodby_sync.sync_service');
    $task_uuid = $syncService->enableInstance($instance_uuid);
    \Drupal::state()->set('wodby_sync.current_task_id', $task_uuid);
    $context['message'] = t('Enabling destination instance after data  sync.');
  }
  catch (Exception $e) {
    $context['results']['errors'][] = $e->getMessage();
  }
}

/**
 * Implements callback_batch_operation().
 *
 * Start the synchronization between two instances.
 *
 * @param string $source_uuid
 *   The uuid of the source instance.
 * @param string $destination_uuid
 *   The uuid of the destination instance.
 * @param array $context
 *   The batch context.
 */
function wodby_sync_batch_start_sync($source_uuid, $destination_uuid, &$context) {
  try {
    /** @var \Drupal\wodby_sync\SyncService $syncService */
    $syncService = \Drupal::service('wodby_sync.sync_service');
    $task_uuid = $syncService->startSync($source_uuid, $destination_uuid);
    \Drupal::state()->set('wodby_sync.current_task_id', $task_uuid);
    $context['message'] = t('Start synchronization');
  }
  catch (Exception $e) {
    $context['results']['errors'][] = $e->getMessage();
  }
}

/**
 * Implements callback_batch_operation().
 *
 * Deploys the instance.
 *
 * @param string $destination_uuid
 *   The uuid of the destination instance.
 * @param array $context
 *   The batch context.
 */
function wodby_sync_batch_deploy_instance($destination_uuid, &$context) {
  try {
    /** @var \Drupal\wodby_sync\SyncService $syncService */
    $syncService = \Drupal::service('wodby_sync.sync_service');
    $task_uuid = $syncService->deployInstance($destination_uuid);
    \Drupal::state()->set('wodby_sync.current_task_id', $task_uuid);
    $context['message'] = t('Start deployment');
  }
  catch (Exception $e) {
    $context['results']['errors'][] = $e->getMessage();
  }
}

/**
 * Implements callback_batch_operation().
 *
 * Waits until a task has finished.
 *
 * @param string $type
 *   The type of the check.
 * @param array $context
 *   The batch context.
 */
function wodby_sync_batch_check_task(string $type, &$context) {
  try {
    /** @var \Drupal\wodby_sync\SyncService $syncService */
    $syncService = \Drupal::service('wodby_sync.sync_service');

    $context['finished'] = 0;

    $task_uuid = \Drupal::state()->get('wodby_sync.current_task_id');

    // Skip processing when there was no current task retrieved.
    if (!is_string($task_uuid)) {
      $context['finished'] = 1;
      return;
    }

    // Define readable types for proper logging.
    switch ($type) {
      case 'sync':
        $readable_type = 'Database synchronization';
        break;
      case 'disable_instance':
        $readable_type = 'Disabling destination instance';
        break;
      case 'enable_instance':
        $readable_type = 'Enabling destination instance';
        break;
      case 'deploy':
        $readable_type = 'Deploy / Post deployment';
        break;
    }

    $task = $syncService->getTask($task_uuid);
    $status = $task['status'];

    switch ($status) {
      case 'in_progress':
      case 'In progress':
        $context['message'] = t('@type: in progress', ['@type' => $readable_type]);
        break;
      case 'Waiting':
      case 'waiting':
        $context['message'] = t('@type: waiting', ['@type' => $readable_type]);
        break;
      case 'done':
      case 'Done':
        $context['finished'] = 1;
        break;
      case 'canceled':
      case 'Canceled':
        \Drupal::logger('wodby_sync')->error('TASK CANCELED: @type', ['@type' => $readable_type]);
        $context['results']['errors'][] = t('Task was canceled: @type', ['@type' => $readable_type]);
        $context['finished'] = 1;
        break;
      case 'failed':
      case 'Failed':
        \Drupal::logger('wodby_sync')->error('TASK FAILED: @type', ['@type' => $readable_type]);
        $context['results']['errors'][] = t('Task has failed: @type', ['@type' => $readable_type]);
        $context['finished'] = 1;
        break;
    }
  }
  catch (Exception $e) {
    $context['results']['errors'][] = $e->getMessage();
  }
}


/**
 * Implements callback_batch_finished().
 *
 * Handles the finish callback after batch completion.
 *
 * @param bool $success
 *   TRUE if batch successfully completed.
 * @param array $results
 *   Batch results.
 * @param $operations
 *   If $success is FALSE, contains the operations that remained unprocessed.
 */
function wodby_sync_batch_finished($success, array $results, $operations) {
  if ($success) {
    if (empty($results['errors'])) {
      \Drupal::messenger()->addStatus('The synchronization process was finished successfully.');
    }
    else {
      foreach ($results['errors'] as $error) {
        \Drupal::messenger()->addError($error);
      }
    }
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ]);
    \Drupal::messenger()->addError($message);
  }
}
