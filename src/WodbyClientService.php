<?php

namespace Drupal\wodby;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class WodblyClientService.
 */
class WodbyClientService implements WodbyClientServiceInterface {

  /**
   * HTTP client for requesting the Wodby API.
   *
   * @var \GuzzleHttp\Client
   */
  protected $wodbyClient;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new WodblyClientService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->wodbyClient = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => 'https://api.wodby.com/api/v3/',
      'Content-Type' => 'application/json',
      'headers' => [
        'X-API-KEY' => $this->getApiKey(),
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getApiKey(): string {
    $apiKey = getenv('WODBY_API_KEY');

    if (empty($apiKey)) {
      throw new \RuntimeException("Missing WODBY_API_KEY in environment.");
    }

    return $apiKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getApps(string $app_id = ''): array {
    $uri = 'apps';
    if (!empty($app_id)) {
      $uri .= '/' . $app_id;
    }
    $response = $this->wodbyClient->get($uri);
    return json_decode($response->getBody()->getContents(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstances(string $app_id = '', string $instance_type = ''): array {
    $options = [];
    if (!empty($app_id)) {
      $options['query']['app_id'] = $app_id;
    }
    if (!empty($instance_type)) {
      $options['query']['type'] = $instance_type;
    }
    $response = $this->wodbyClient->get('instances', $options);
    return json_decode($response->getBody()->getContents(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getTasks(string $task_id = ''): array {
    $uri = 'tasks';
    if (!empty($task_id)) {
      $uri .= '/' . $task_id;
    }
    $response = $this->wodbyClient->get($uri);
    return json_decode($response->getBody()->getContents(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function deployInstance(string $instance_id, array $data = []): array {
    $response = $this->wodbyClient->request('POST', 'instances/' . $instance_id . '/deploy', ['json' => $data]);
    return json_decode($response->getBody()->getContents(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function deployInstanceCodebase(string $instance_id, array $data = []): array {
    $data = ['post_deployment' => TRUE, 'git' => ['tree_ish' => 'master']];
    $response = $this->wodbyClient->request('POST', 'instances/' . $instance_id . '/deploy-codebase', ['json' => $data]);
    return json_decode($response->getBody()->getContents(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getAppDrushAliases(string $app_id): string {
    $response = $this->wodbyClient->get('apps/' . $app_id . '/drush-aliases');
    return $response->getBody()->getContents();
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteAppUuid(): ?string {
    return $this->settings()->get('app_uuid');
  }

  /**
   * {@inheritdoc}
   */
  public function setSiteAppUuid(string $uuid) {
    $this->settings(TRUE)
      ->set('app_uuid', $uuid)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuickDeployList(): array {
    $list = $this->settings()->get('instances.quick_deploy');
    return is_array($list) ? $list : [];
  }

  /**
   * Get the module's configuration.
   *
   * @param bool $editable
   *   True or false param.
   *
   * @return \Drupal\Core\Config\Config
   *   The wodby settings config.
   */
  protected function settings($editable = FALSE): Config {
    if ($editable) {
      return $this->configFactory->getEditable('wodby.settings');
    }

    return $this->configFactory->get('wodby.settings');
  }

}
