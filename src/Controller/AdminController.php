<?php

namespace Drupal\wodby\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;

class AdminController {

  use StringTranslationTrait;

  public function index() {
    $build = [];

    $build['wodby_sync_admin_description'] = [
      '#markup' => $this->t('This page shows the status of the Wodby Sync module.'),
    ];

    $build['env'] = [
      '#type' => 'details',
      '#title' => 'Environment information',
      '#collapsible' => TRUE,
    ];

    $build['env']['wodby_app'] = [
      '#type' => 'item',
      '#title' => $this->t('Application'),
      '#markup' => $this->t('@app_name (@uuid)', ['@app_name' => getenv('WODBY_APP_NAME'), '@uuid' => getenv('WODBY_APP_UUID')]),
    ];

    $build['env']['wodby_instance'] = [
      '#type' => 'item',
      '#title' => 'Instance',
      '#markup' => $this->t('@instance_name (@uuid)', ['@instance_name' => getenv('WODBY_INSTANCE_NAME'), '@uuid' => getenv('WODBY_INSTANCE_UUID')]),
    ];

    $build['env']['wodby_instance_type'] = [
      '#type' => 'item',
      '#title' => 'Environment',
      '#markup' => getenv('WODBY_INSTANCE_TYPE'),
    ];

    $build['env']['wodby_url_primary'] = [
      '#type' => 'item',
      '#title' => 'Primary URL',
      '#markup' => getenv('WODBY_URL_PRIMARY'),
    ];

    preg_match_all('/[\[,]([a-z0-9.]+)[,\]]/i', getenv('WODBY_HOSTS'), $hosts);
    $build['env']['wodby_hosts'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('Valid hostnames'),
      '#items' => $hosts[1],
    ];

    $build['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API information'),
    ];

    try {
      /** @var \Drupal\wodby\WodbyClientServiceInterface $service */
      $service = \Drupal::service('wodby.client');
      $app_id = getenv('WODBY_APP_UUID');
      $instances =  $service->getInstances($app_id);

      foreach ($instances as $i => $instance) {
        $build['api']['instance_'.$i] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Instance: @name', ['@name' => $instance['title']]),
        ];
        $build['api']['instance_'.$i]['instance_id'] = [
          '#type' => 'item',
          '#title' => 'UUID',
          '#markup' => $instance['id'],
        ];
        $build['api']['instance_'.$i]['instance_name'] = [
          '#type' => 'item',
          '#title' => 'Instance name',
          '#markup' => $instance['name'],
        ];
        $build['api']['instance_'.$i]['instance_type'] = [
          '#type' => 'item',
          '#title' => 'Instance type',
          '#markup' => $instance['type'],
        ];
      }
    }
    catch (\RuntimeException $e) {
      $build['api']['error'] = [
        '#type' => 'item',
        '#title' => 'Error',
        '#markup' => $e->getMessage(),
      ];
    }

    return $build;
  }
}
