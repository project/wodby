<?php

namespace Drupal\wodby;

/**
 * Interface WodblyClientServiceInterface.
 */
interface WodbyClientServiceInterface {

  /**
   * Get the Wodby API key.
   *
   * @return string
   *   The Wodby API key.
   */
  public function getApiKey(): string;

  /**
   * @param string $app_id
   *   The application id to retrieve.
   *
   * @return array
   *   The response as an array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getApps(string $app_id = ''): array;

  /**
   * Get the instances from Wodby.
   *
   * @param string $app_id
   *   The application id to retrieve instances for.
   * @param string $instance_type
   *   The instance type to filter for.
   *
   * @return array
   *   The response as an array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getInstances(string $app_id = '', string $instance_type = ''): array;

  /**
   * Get tasks from Wodby.
   *
   * @param string $task_id
   *   Optional add a task id to retrieve a specific task.
   *
   * @return array
   *   The response as an array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTasks(string $task_id = ''): array;

  /**
   * Get drush aliases for the given app.
   *
   * @param string $app_id
   *   The app id.
   *
   * @return string
   *   The drush alias YAML data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAppDrushAliases(string $app_id): string;

  /**
   * (Re)deploy an instance.
   *
   * @param string $instance_id
   *   The instance id to be deployed.
   * @param array $data
   *   The data to be posted.
   *
   * @return array
   *   The response as an array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deployInstance(string $instance_id, array $data = []): array;

  /**
   * (Re)deploy an instance codebase.
   *
   * Only works for git-based instances.
   *
   * @param string $instance_id
   *   The instance id to be deployed.
   * @param array $data
   *   The data to be posted.
   *
   * @return array
   *   The response as an array.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function deployInstanceCodebase(string $instance_id, array $data = []): array;

  /**
   * Get the App UUID.
   *
   * @return null|string
   *   Returns the UUID, if it exists.
   */
  public function getSiteAppUuid(): ?string;

  /**
   * Stores in the config the App UUID.
   *
   * @param string $uuid
   *   The uuid.
   */
  public function setSiteAppUuid(string $uuid);

  /**
   * Gets a list of Instance UUIDs to use for quick deploy.
   *
   * @return string[]
   *   An array of strings.
   */
  public function getQuickDeployList(): array;

}
