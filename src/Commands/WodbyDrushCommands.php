<?php

namespace Drupal\wodby\Commands;

// @noinspection PhpUnusedParameterInspection, PhpDocSignatureInspection, PhpDocMissingReturnTagInspection
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\wodby\WodbyClientServiceInterface;
use Drush\Commands\DrushCommands;
use GuzzleHttp\Exception\GuzzleException;

/**
 * A Drush commandfile.
 */
class WodbyDrushCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  protected const DEFAULT_OPTS = [
    'api-key' => self::REQ,
  ];

  /**
   * The client service.
   *
   * @var \Drupal\wodby\WodbyClientServiceInterface
   */
  protected $wodbyClientService;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(FileSystemInterface $file_system, DateFormatterInterface $date_formatter) {
    $this->fileSystem = $file_system;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Gets the Wodby client service.
   *
   * Not dependency inject the class, because it will cause an exception
   * if the required environment variables have not been specified.
   *
   * @return \Drupal\wodby\WodbyClientServiceInterface|void
   */
  protected function getWodbyClient() {
    if (is_null($this->wodbyClientService)) {
      $this->wodbyClientService = \Drupal::service('wodby.client');
    }

    return $this->wodbyClientService;
  }

  /**
   * Downloads the alias file to DRUPAL_ROOT/..drush/sites/self.site.yml.
   *
   * @param string $app_uuid
   *   Optional App UUID from Wodby. If not provided will be looked up in the
   *   config, if not available will be prompted.
   * @param array $opts
   *   The default opts constant.
   *
   * @command wodby:alias-download
   * @aliases w:dl
   *
   * @option skip-file-save
   *    Do not save the file. Useful for testing.
   *
   * @usage wodby:alias-download
   *   Downloads the alias file, the App UUID must be set in config or via
   * the environment variable WODBY_APP_UUID
   * @usage wodby:alias-download 'a234-abd1-f34a5-7597-foo'
   *   Downloads the alias for the specified app, disregarding the current
   * site configuration.
   */
  public function downloadDrushAlias(
    string $app_uuid = NULL,
    array $opts = self::DEFAULT_OPTS
  ) {
    $app_uuid = $app_uuid ?: $this->getCurrentAppUuid();
    $alias_file_data = $this->getWodbyClient()->getAppDrushAliases($app_uuid);
    if (!$opts['skip-file-save']) {
      $alias_file_path = realpath(DRUPAL_ROOT . '/../drush/sites/self.site.yml');
      if (!$this->fileSystem->saveData($alias_file_data, $alias_file_path, FileSystemInterface::EXISTS_REPLACE)) {
        $this->logger()->error('Could not write to drush alias file.');
        return FALSE;
      }
    }

    $this->logger()->notice('Alias file updated.');
  }

  /**
   * Sets the App UUID for the current site.
   *
   * @param string $app_uuid
   *   App UUID from Wodby.
   * @param array $opts
   *   The default opts constant.
   *
   * @command wodby:set-app-uuid
   * @aliases w:suid
   *
   * @option skip-api-check
   *   If specified will not check if the UUID is valid against the API.
   *
   * @usage wodby:set-app-uuid 'a234-abd1-f34a5-7597-foo'
   *   Sets the App UUID in the configuration of the current site.
   */
  public function setAppUuid(
    string $app_uuid,
    array $opts = [
      'skip-api-check' => FALSE,
    ] + self::DEFAULT_OPTS
  ) {
    if (!$opts['skip-api-check']) {
      try {
        /*
         * Try to load the App details via the API, if the app does not exist
         * it wil throw a ApiException.
         */
        $selected_app = $this->getWodbyClient()->getApps($app_uuid);
      }
      catch (GuzzleException $exception) {
        if ($exception->getCode() === 404) {
          $this->logger()->error(
            new TranslatableMarkup(
              'App does not exist on Wodby or current Api Key does not allow you access. If you want to add this anyway use the --skip-api-check option.'
            )
          );
          // Exit.
          return FALSE;
        }

        // Unhandled exception should be propagated.
        throw $exception;
      }
    }

    $this->getWodbyClient()->setSiteAppUuid($app_uuid);
    /*
     * Print a nice message if we have the info from the API.
     * If not simply fallback to print the app UUID
     */
    if (isset($selected_app)) {
      $message = new TranslatableMarkup(
        'Configuration updated. Selected App: "@label" (Name: @name / UUID: @id)',
        [
          '@label' => $selected_app['title'],
          '@name'  => $selected_app['name'],
          '@id'    => $selected_app['id'],
        ]
      );

    }
    else {
      $message = new TranslatableMarkup(
        'Configuration updated with new UUID: @id', ['@id' => $app_uuid]
      );
    }
    $this->logger()->notice($message);
  }

  /**
   * Deploys a instance of a specific type.
   *
   * @param string $instance_type
   *   Should be one of: "prod", "stage", "dev". (default: dev)
   * @param array $opts
   *   The default opts constant.
   *
   * @command wodby:deploy
   * @aliases w:d
   *
   * @option ignore-task-status
   *   If specified it will not wait for the task completion.
   *
   * @usage wodby:deploy
   *   Deploys the
   * @usage wodby:deploy prod
   *   Deploys the Prod
   *
   * @throws \Drush\Exceptions\UserAbortException
   */
  public function deployInstance(
    string $instance_type = 'dev',
    array $opts = [
      'ignore-task-status' => FALSE,
    ] + self::DEFAULT_OPTS
  ) {
    $allowed_types = ['dev', 'stage', 'prod'];
    if (!in_array($instance_type, $allowed_types)) {
      $this->logger()->error(
        new TranslatableMarkup('Instance type must be one of: @types', ['@types' => implode(', ', $allowed_types)])
      );
      // Exit.
      return FALSE;
    }

    $app_uuid = $this->getCurrentAppUuid();
    $instances = $this->getWodbyClient()->getInstances($app_uuid, $instance_type);

    if (empty($instances)) {
      throw new \RuntimeException(
        strtr(
          'No instances of type  found for app: @app',
          ['@type' => $instance_type, '@app' => $app_uuid]
        )
      );
    }

    $instance = NULL;
    // If we only have 1, select the first.
    if (count($instances) === 1) {
      $instance = reset($instances);
    }

    if (!$instance) {
      $instances_choice = $this->getChoicesFromInstanceArray($instances);
      $choice = $this->io()->choice('Multiple instances found, select one: ', $instances_choice);
      $instance = $instances[$choice] ?? NULL;
    }

    try {
      $task = $this->getWodbyClient()->deployInstanceCodebase($instance['id']);
      $task = $task['task'];
    }
    catch (GuzzleException $exception) {
      throw new \RuntimeException(
        'Unable to deploy codebase.',
        $exception->getCode(),
        $exception
      );
    }

    $this->logger()->notice(
      new TranslatableMarkup(
        'Deploy task started (ID: @id).',
        ['@id' => $task['id']]
      )
    );

    if (!$opts['ignore-task-status']) {
      // Start wait bar.
      $this->io()->progressStart();
      // Keep track of elapsed time.
      $elapsed_time = 0;
      while ($task['end'] === 0) {
        // Refresh task info.
        $task = $this->getWodbyClient()->getTasks($task['id']);

        $status = $task['status'];
        if ($status === 'done') {
          break;
        }
        elseif ($status === 'canceled') {
          $this->logger()->warning(
            new TranslatableMarkup('Deploy task canceled.')
          );
          break;
        }
        elseif ($status === 'failed') {
          $this->logger()->error(
            new TranslatableMarkup('Deploy task failed, please check Wodby UI for more info.')
          );
          break;
        }
        elseif ($elapsed_time >= 300) {
          $this->logger()->warning(
            new TranslatableMarkup('Deploy operation is taking to long, giving up.')
          );
          break;
        }
        else {
          sleep(10);
          // Step forward.
          $this->io()->progressAdvance();
          // Add the elapsed time.
          $elapsed_time += 10;
        }
      }
      // Mark completed.
      $this->io()->progressFinish();

      if ($task['status'] === 'done') {
        // Log the end time.
        $this->logger()->notice(
          new TranslatableMarkup(
            'Deploy task completed at: @end',
            ['@end' => $this->dateFormatter->format($task['end'])]
          )
        );
      }
    }
  }

  /**
   * Quick deploy pre-defined instances.
   *
   * @command wodby:quick-deploy
   * @aliases w:ddd
   *
   * @usage w:qdd
   */
  public function quickDeploy(
    $opts = self::DEFAULT_OPTS
  ) {
    $quick_deploy = $this->getWodbyClient()->getQuickDeployList();
    if (empty($quick_deploy)) {
      $this->logger()->warning(
        new TranslatableMarkup('Quick-deploy list is empty.')
      );
      // Exit nicely.
      return;
    }

    $tasks = [];
    // Start wait bar.
    $this->io()->progressStart(count($quick_deploy));
    foreach ($quick_deploy as $instance_id) {
      try {
        $tasks[$instance_id] = $this->getWodbyClient()->deployInstanceCodebase($instance_id);
      }
      catch (GuzzleException $e) {
        $this->logger()->error(
          new TranslatableMarkup(
            'Could not deploy instance: @id (Error: @code)',
            ['@id' => $instance_id, '@code' => $e->getCode()]
          )
        );
      }
      // Advance step.
      $this->io()->progressAdvance();
    }
    // Loop done.
    $this->io()->progressFinish();

    // Optionally check the status of each item.
    if ($opts['follow-task-status']) {
      // @todo Implement status check on $tasks
      $this->logger()->warning(
        new TranslatableMarkup('Options not implemented: --follow-task-status.')
      );
    }

    $this->logger()->notice(
      new TranslatableMarkup('All deployment requests are done.')
    );
  }

  /**
   * Gets the current app UUID from environment ot from config.
   *
   * @return string
   *   The app UUID.
   */
  protected function getCurrentAppUuid(): string {
    // First try to get from the env.
    $app_uuid = getenv('WODBY_APP_UUID');
    if ($app_uuid) {
      return $app_uuid;
    }

    $app_uuid = $this->getWodbyClient()->getSiteAppUuid();
    if ($app_uuid) {
      return $app_uuid;
    }

    throw new \RuntimeException('Missing App UUID from env and config.');
  }

  /**
   * Given array of instances builds a choice array to be used as IO::choice()
   *
   * @param array $instances
   *   An array of instances.
   *
   * @return array
   *   An array of choices.
   */
  protected function getChoicesFromInstanceArray(array $instances): array {
    $choices = [];
    foreach ($instances as $key => $instance) {
      $choices[$key] = strtr(
        '@name (@uuid)',
        ['@name' => $instance['name'], '@uuid' => $instance['id']]
      );
    }
    return $choices;
  }

}
