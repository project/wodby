<?php

namespace Drupal\wodby_test;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Service Provider to overwrite the Wodby Client Service for testing.
 */
class WodbyTestServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('wodby.client')
      ->setClass(TestWodbyClientService::class);
  }

}
