<?php

namespace Drupal\wodby_test;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\wodby\WodbyClientService;
use GuzzleHttp\Client;

/**
 * A test implementation of the WodbyClientService for testing purposes.
 */
class TestWodbyClientService extends WodbyClientService {

  /**
   * Constructs a new WodblyClientService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->wodbyClient = new Client();
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteAppUuid(): ?string {
    return 'abc';
  }

  /**
   * {@inheritdoc}
   */
  public function getApps(string $app_id = ''): array {
    return json_decode(file_get_contents(__DIR__ . '/../../../fixtures/app.json'), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getAppDrushAliases(string $app_id): string {
    return file_get_contents(__DIR__ . '/../../../fixtures/drush-aliases.yml');
  }

  /**
   * {@inheritdoc}
   */
  public function getInstances(string $app_id = '', string $instance_type = ''): array {
    return json_decode(file_get_contents(__DIR__ . '/../../../fixtures/instances.json'), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function deployInstanceCodebase(string $instance_id, array $data = []): array {
    return json_decode(file_get_contents(__DIR__ . '/../../../fixtures/deploy-codebase.json'), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getTasks(string $task_id = ''): array {
    return json_decode(file_get_contents(__DIR__ . '/../../../fixtures/task.json'), TRUE);
  }

}
