<?php

namespace Drupal\Tests\wodby\Functional;

use Drupal\Tests\BrowserTestBase;
use Drush\Drush;
use Drush\TestTraits\DrushTestTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Class to test Drush commands related to the Wodby module.
 */
class DrushCommandsTest extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'wodby',
    'wodby_test',
  ];

  protected $mockHandler;

  protected function setUp(): void {
    parent::setUp();

    putenv('WODBY_API_KEY=ABC');
    $_ENV['WODBY_API_KEY'] = 'ABC';
  }

  /**
   * Tests the 'wodby:alias-download' Drush command.
   *
   * @return void
   */
  public function testAliasDownload() {
    // Drush 11 does not support properly value-less option testing. Therefore,
    // skipping the test for Drush 11 as long as we support it.
    if (version_compare(Drush::getVersion(), '12.0.0', '<')) {
      $this->markTestSkipped('Drush 11 does not support value-less option testing.');
    }
    $this->drush('wodby:alias-download', [], ['skip-file-save' => TRUE]);
  }

  /**
   * Tests the 'wodby:deploy' Drush command.
   *
   * @return void
   */
  public function testDeploy() {
    $this->drush('wodby:deploy');
  }

  /**
   * Tests the 'wodby:quick-deploy' Drush command.
   *
   * @return void
   */
  public function testQuickDeploy() {
    $this->drush('wodby:quick-deploy');
  }

  /**
   * Tests the 'wodby:set-app-uuid' Drush command.
   *
   * @return void
   */
  public function testSetAppUuid() {
    $this->drush('wodby:set-app-uuid', ['abc']);
  }

}
